import React, { Component } from 'react'

const NumbersSam = (props) => {
    const num = props.sam.map((val,index)=>{
        return <p key={index}> value: {val*val}</p>
        
    })
    return num;

}


export default class Numbers extends Component {
  render() {
    const {sam} = this.props;

    return (
        <NumbersSam sam ={sam} />
      
    )
  }
}

