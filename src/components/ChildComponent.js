import React, { Component } from 'react'

//simple component or Function Component
const Child = (props) =>{
  return <p>{props.text}</p>
}


//Class Component
export default class ChildComponent extends Component {
  render() {
    const {text} = this.props;

    return (
      <Child text = {text}/>
    )
  }
}


