import React, { Component } from 'react'
import Table from 'react-bootstrap/Table'


const TableHeader = () => {
  return(
    <thead>
    <tr>
      <th>First Name</th>
      <th>Last Name</th>
    </tr>
  </thead>
  )
};
const TableBody = (props) => {
  const rows = props.names.map((row, index)=>{
    return(
      <tr key = {index}>
        <td>{row.fname}</td>
         <td>{row.lname}</td>
      </tr>
    )
  })
  return (
  <tbody>{rows}</tbody>
  )
}
export default class table extends Component {
  render() {
    const {names} = this.props;

    return (
      <Table striped bordered hover>
        <TableHeader />
        <TableBody names={names} />
      </Table>
   
    )
  }
}
